<?php
class DemandesFormations{
	private array $formations ;

	public function __construct($array){
		if (is_array($array)) {
			$this->formations = $array;
		}
	}

	public function getDemandesFormation(){
		return $this->formations;
	}


	public function chercheDemandeFormation($unIDFormation){
		$i = 0;
		while ($unIDFormation != $this->formations[$i]->getIDFORMA() && $i < count($this->formations)-1){
			$i++;
		}
		if ($unIDFormation == $this->formations[$i]->getIDFORMA()){
			return $this->formations[$i];
		}
	}
}