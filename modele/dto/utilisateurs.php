<?php
class Utilisateurs{
    private array $utilisateurs;


    public function __construct($array){
		if (is_array($array)) {
			$this->utilisateurs = $array;
		}
	}

	public function getUtilisateur(){
		return $this->utilisateurs;
	}

	public function chercheUtilisateur($unID){
		$i = 0;
		while ($unID != $this->utilisateurs[$i]->getIDUSER() && $i < count($this->utilisateurs)-1){
			$i++;
		}
		if ($unID == $this->utilisateurs[$i]->getIDUSER()){
			return $this->utilisateurs[$i];
		}
	}
}