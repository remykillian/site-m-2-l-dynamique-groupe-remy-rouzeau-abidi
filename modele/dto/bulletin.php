<?php
class Bulletin{
    use Hydrate;
    private ?string $IDBULLETIN;
	private ?string $IDCONTRAT;
	private ?string $MOIS;
	private ?string $ANNEE;
	private ?string $BULLETINPDF;

    public function __construct(?int $unIDBULLETIN){
	    $this->IDBULLETIN = $unIDBULLETIN;
    }
    /**
     * Get the value of IDBULLETIN
     */ 
    public function getIDBULLETIN()
    {
        return $this->IDBULLETIN;
    }

    /**
     * Set the value of IDBULLETIN
     *
     * @return  self
     */ 
    public function setIDBULLETIN($IDBULLETIN)
    {
        $this->IDBULLETIN = $IDBULLETIN;

        return $this;
    }

	/**
	 * Get the value of IDCONTRAT
	 */ 
	public function getIDCONTRAT()
	{
		return $this->IDCONTRAT;
	}

	/**
	 * Set the value of IDCONTRAT
	 *
	 * @return  self
	 */ 
	public function setIDCONTRAT($IDCONTRAT)
	{
		$this->IDCONTRAT = $IDCONTRAT;

		return $this;
	}

	/**
	 * Get the value of MOIS
	 */ 
	public function getMOIS()
	{
		return $this->MOIS;
	}

	/**
	 * Set the value of MOIS
	 *
	 * @return  self
	 */ 
	public function setMOIS($MOIS)
	{
		$this->MOIS = $MOIS;

		return $this;
	}

	/**
	 * Get the value of ANNEE
	 */ 
	public function getANNEE()
	{
		return $this->ANNEE;
	}

	/**
	 * Set the value of ANNEE
	 *
	 * @return  self
	 */ 
	public function setANNEE($ANNEE)
	{
		$this->ANNEE = $ANNEE;

		return $this;
	}

	/**
	 * Get the value of BULLETINPDF
	 */ 
	public function getBULLETINPDF()
	{
		return $this->BULLETINPDF;
	}

	/**
	 * Set the value of BULLETINPDF
	 *
	 * @return  self
	 */ 
	public function setBULLETINPDF($BULLETINPDF)
	{
		$this->BULLETINPDF = $BULLETINPDF;

		return $this;
	}
	}
