<?php
class Utilisateur{
    use hydrate;
    private ?int $IDUSER;
    private ?int $IDFONCT;
    private ?int $IDCLUB;
    private ?int $IDLIGUE;
    private ?string $NOM;
    private ?string $PRENOM;
    private ?string $LOGIN;
    private ?string $MDP;
    private ?string $TYPEUSER;

    public function __construct(?int $IDUSER  , ?string $NOM){
		$this->NOM = $NOM;
		$this->IDUSER = $IDUSER;
	}

    /**
     * Get the value of TYPEUSER
     */ 
    public function getTYPEUSER()
    {
        return $this->TYPEUSER;
    }

    /**
     * Set the value of TYPEUSER
     *
     * @return  self
     */ 
    public function setTYPEUSER($TYPEUSER)
    {
        $this->TYPEUSER = $TYPEUSER;

        return $this;
    }

    /**
     * Get the value of MDP
     */ 
    public function getMDP()
    {
        return $this->MDP;
    }

    /**
     * Set the value of MDP
     *
     * @return  self
     */ 
    public function setMDP($MDP)
    {
        $this->MDP = $MDP;

        return $this;
    }

    /**
     * Get the value of LOGIN
     */ 
    public function getLOGIN()
    {
        return $this->LOGIN;
    }

    /**
     * Set the value of LOGIN
     *
     * @return  self
     */ 
    public function setLOGIN($LOGIN)
    {
        $this->LOGIN = $LOGIN;

        return $this;
    }

    /**
     * Get the value of PRENOM
     */ 
    public function getPRENOM()
    {
        return $this->PRENOM;
    }

    /**
     * Set the value of PRENOM
     *
     * @return  self
     */ 
    public function setPRENOM($PRENOM)
    {
        $this->PRENOM = $PRENOM;

        return $this;
    }

    /**
     * Get the value of NOM
     */ 
    public function getNOM()
    {
        return $this->NOM;
    }

    /**
     * Set the value of NOM
     *
     * @return  self
     */ 
    public function setNOM($NOM)
    {
        $this->NOM = $NOM;

        return $this;
    }

    /**
     * Get the value of IDLIGUE
     */ 
    public function getIDLIGUE()
    {
        return $this->IDLIGUE;
    }

    /**
     * Set the value of IDLIGUE
     *
     * @return  self
     */ 
    public function setIDLIGUE($IDLIGUE)
    {
        $this->IDLIGUE = $IDLIGUE;

        return $this;
    }

    /**
     * Get the value of IDCLUB
     */ 
    public function getIDCLUB()
    {
        return $this->IDCLUB;
    }

    /**
     * Set the value of IDCLUB
     *
     * @return  self
     */ 
    public function setIDCLUB($IDCLUB)
    {
        $this->IDCLUB = $IDCLUB;

        return $this;
    }

    /**
     * Get the value of IDFONCT
     */ 
    public function getIDFONCT()
    {
        return $this->IDFONCT;
    }

    /**
     * Set the value of IDFONCT
     *
     * @return  self
     */ 
    public function setIDFONCT($IDFONCT)
    {
        $this->IDFONCT = $IDFONCT;

        return $this;
    }

    /**
     * Get the value of IDUSER
     */ 
    public function getIDUSER()
    {
        return $this->IDUSER;
    }

    /**
     * Set the value of IDUSER
     *
     * @return  self
     */ 
    public function setIDUSER($IDUSER)
    {
        $this->IDUSER = $IDUSER;

        return $this;
    }
}