<?php
class Formation{
    use hydrate;
	private ?int $IDFORMA;
	private ?string $INTITULE;
	private ?string $DESCRIPTIF;
	private ?string $DUREE;
	private ?string $DATEOUVERTUREINSCRIPTION;
	private ?string $DATECLOTUREINSCRIPTION;
	private ?int $EFFECTIF;

	public function __construct(?int $IDFORMA  , ?string $INTITULE){
		$this->INTITULE = $INTITULE;
		$this->IDFORMA = $IDFORMA;
	}


	/**
	 * Get the value of EFFECTIF
	 */ 
	public function getEFFECTIF()
	{
		return $this->EFFECTIF;
	}

	/**
	 * Set the value of EFFECTIF
	 *
	 * @return  self
	 */ 
	public function setEFFECTIF($EFFECTIF)
	{
		$this->EFFECTIF = $EFFECTIF;

		return $this;
	}

	/**
	 * Get the value of DATECLOTUREINSCRIPTION
	 */ 
	public function getDATECLOTUREINSCRIPTION()
	{
		return $this->DATECLOTUREINSCRIPTION;
	}

	/**
	 * Set the value of DATECLOTUREINSCRIPTION
	 *
	 * @return  self
	 */ 
	public function setDATECLOTUREINSCRIPTION($DATECLOTUREINSCRIPTION)
	{
		$this->DATECLOTUREINSCRIPTION = $DATECLOTUREINSCRIPTION;

		return $this;
	}

	/**
	 * Get the value of DATEOUVERTUREINSCRIPTION
	 */ 
	public function getDATEOUVERTUREINSCRIPTION()
	{
		return $this->DATEOUVERTUREINSCRIPTION;
	}

	/**
	 * Set the value of DATEOUVERTUREINSCRIPTION
	 *
	 * @return  self
	 */ 
	public function setDATEOUVERTUREINSCRIPTION($DATEOUVERTUREINSCRIPTION)
	{
		$this->DATEOUVERTUREINSCRIPTION = $DATEOUVERTUREINSCRIPTION;

		return $this;
	}

	/**
	 * Get the value of DUREE
	 */ 
	public function getDUREE()
	{
		return $this->DUREE;
	}

	/**
	 * Set the value of DUREE
	 *
	 * @return  self
	 */ 
	public function setDUREE($DUREE)
	{
		$this->DUREE = $DUREE;

		return $this;
	}

	/**
	 * Get the value of DESCRIPTIF
	 */ 
	public function getDESCRIPTIF()
	{
		return $this->DESCRIPTIF;
	}

	/**
	 * Set the value of DESCRIPTIF
	 *
	 * @return  self
	 */ 
	public function setDESCRIPTIF($DESCRIPTIF)
	{
		$this->DESCRIPTIF = $DESCRIPTIF;

		return $this;
	}

	/**
	 * Get the value of INTITULE
	 */ 
	public function getINTITULE()
	{
		return $this->INTITULE;
	}

	/**
	 * Set the value of INTITULE
	 *
	 * @return  self
	 */ 
	public function setINTITULE($INTITULE)
	{
		$this->INTITULE = $INTITULE;

		return $this;
	}

	/**
	 * Get the value of IDFORMA
	 */ 
	public function getIDFORMA()
	{
		return $this->IDFORMA;
	}

	/**
	 * Set the value of IDFORMA
	 *
	 * @return  self
	 */ 
	public function setIDFORMA($IDFORMA)
	{
		$this->IDFORMA = $IDFORMA;

		return $this;
	}
}