<?php
class Intervenant{
    use Hydrate;
	private ?string $IDUSER;
	private ?string $IDFONCT;
	private CLUB $CLUB;
	private LIGUE $LIGUE;
	private ?string $NOM;
    private ?string $PRENOM;
	private ?string $LIBELLE;
	private ?string $NOMFONCT;
	private ?string $IDCLUB;
	private ?string $IDLIGUE;
	

	public function __construct(?int $unidUser){
	    $this->IDUSER = $unidUser;
	}
	

	public function getClub(){
		return $this->CLUB;
	}

	public function getLigue(){
		return $this->LIGUE;
	}
	
	public function setClub(CLUB $unClub){
			$this->CLUB = $unClub;
	}

	public function setLigue(LIGUE $unLigue){
		$this->LIGUE = $unLigue;
	}

	/**
	 * Get the value of idUser
	 */ 
	public function getIdUser()
	{
		return $this->IDUSER;
	}

	/**
	 * Set the value of idUser
	 *
	 * @return  self
	 */ 
	public function setIdUser($idUser)
	{
		$this->IDUSER = $idUser;

		return $this;
	}

	/**
	 * Get the value of idFonction
	 */ 
	public function getIdFonction()
	{
		return $this->IDFONCT;
	}

	/**
	 * Set the value of IDFONCTion
	 *
	 * @return  self
	 */ 
	public function setIdFonction($idFonct)
	{
		$this->IDFONCT = $idFonct;

		return $this;
	}

	/**
	 * Get the value of idClub
	 */ 
	public function getIdClub()
	{
		return $this->IDCLUB;
	}

	/**
	 * Set the value of idClub
	 *
	 * @return  self
	 */ 
	public function setIdClub($idClub)
	{
		$this->IDCLUB = $idClub;

		return $this;
	}



	/**
	 * Get the value of Nom
	 */ 
	public function getNom()
	{
		return $this->NOM;
	}

	/**
	 * Set the value of Nom
	 *
	 * @return  self
	 */ 
	public function setNom($Nom)
	{
		$this->NOM = $Nom;

		return $this;
	}

    /**
     * Get the value of Prenom
     */ 
    public function getPrenom()
    {
        return $this->PRENOM;
    }

    /**
     * Set the value of Prenom
     *
     * @return  self
     */ 
    public function setPrenom($Prenom)
    {
        $this->PRENOM = $Prenom;

        return $this;
    }

	/**
	 * Get the value of LIBELLE
	 */ 
	public function getLIBELLE()
	{
		return $this->LIBELLE;
	}

	/**
	 * Set the value of LIBELLE
	 *
	 * @return  self
	 */ 
	public function setLIBELLE($LIBELLE)
	{
		$this->LIBELLE = $LIBELLE;

		return $this;
	}

	/**
	 * Get the value of IDLIGUE
	 */ 
	public function getIDLIGUE()
	{
		return $this->IDLIGUE;
	}

	/**
	 * Set the value of IDLIGUE
	 *
	 * @return  self
	 */ 
	public function setIDLIGUE($IDLIGUE)
	{
		$this->IDLIGUE = $IDLIGUE;

		return $this;
	}

	/**
	 * Get the value of NOMFONCT
	 */ 
	public function getNOMFONCT()
	{
		return $this->NOMFONCT;
	}

	/**
	 * Set the value of NOMFONCT
	 *
	 * @return  self
	 */ 
	public function setNOMFONCT($NOMFONCT)
	{
		$this->NOMFONCT = $NOMFONCT;

		return $this;
	}
}