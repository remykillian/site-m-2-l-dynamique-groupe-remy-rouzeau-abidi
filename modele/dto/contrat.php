<?php
class Contrat{
    use Hydrate;
    private ?string $IDCONTRAT;
	private ?string $IDUSER;
	private ?string $DATEDEBUT;
	private ?string $DATEFIN;
	private ?string $TYPECONTRAT;
    private ?string $NBHEURES;


	public function __construct(?int $unIDCONTRAT){
	    $this->IDCONTRAT = $unIDCONTRAT;
	}

    /**
     * Get the value of IDCONTRAT
     */ 
    public function getIDCONTRAT()
    {
        return $this->IDCONTRAT;
    }

    /**
     * Set the value of IDCONTRAT
     *
     * @return  self
     */ 
    public function setIDCONTRAT($IDCONTRAT)
    {
        $this->IDCONTRAT = $IDCONTRAT;

        return $this;
    }

	/**
	 * Get the value of IDUSER
	 */ 
	public function getIDUSER()
	{
		return $this->IDUSER;
	}

	/**
	 * Set the value of IDUSER
	 *
	 * @return  self
	 */ 
	public function setIDUSER($IDUSER)
	{
		$this->IDUSER = $IDUSER;

		return $this;
	}

	/**
	 * Get the value of DATEDEBUT
	 */ 
	public function getDATEDEBUT()
	{
		return $this->DATEDEBUT;
	}

	/**
	 * Set the value of DATEDEBUT
	 *
	 * @return  self
	 */ 
	public function setDATEDEBUT($DATEDEBUT)
	{
		$this->DATEDEBUT = $DATEDEBUT;

		return $this;
	}

	/**
	 * Get the value of DATEFIN
	 */ 
	public function getDATEFIN()
	{
		return $this->DATEFIN;
	}

	/**
	 * Set the value of DATEFIN
	 *
	 * @return  self
	 */ 
	public function setDATEFIN($DATEFIN)
	{
		$this->DATEFIN = $DATEFIN;

		return $this;
	}

	/**
	 * Get the value of TYPECONTRAT
	 */ 
	public function getTYPECONTRAT()
	{
		return $this->TYPECONTRAT;
	}

	/**
	 * Set the value of TYPECONTRAT
	 *
	 * @return  self
	 */ 
	public function setTYPECONTRAT($TYPECONTRAT)
	{
		$this->TYPECONTRAT = $TYPECONTRAT;

		return $this;
	}

    /**
     * Get the value of NBHEURES
     */ 
    public function getNBHEURES()
    {
        return $this->NBHEURES;
    }

    /**
     * Set the value of NBHEURES
     *
     * @return  self
     */ 
    public function setNBHEURES($NBHEURES)
    {
        $this->NBHEURES = $NBHEURES;

        return $this;
    }
}