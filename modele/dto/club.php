<?php
class Club{
    use Hydrate;
    private ?string $IDCLUB;
	private ?string $IDLIGUE;
	private ?string $IDCOMMUNE;
	private ?string $NOMCLUB;
	private ?string $ADRESSECLUB;

	public function __construct(?int $unidClub){
	    $this->IDCLUB = $unidClub;
	}
	

	/**
	 * Get the value of ADRESSECLUB
	 */ 
	public function getADRESSECLUB()
	{
		return $this->ADRESSECLUB;
	}

	/**
	 * Set the value of ADRESSECLUB
	 *
	 * @return  self
	 */ 
	public function setADRESSECLUB($ADRESSECLUB)
	{
		$this->ADRESSECLUB = $ADRESSECLUB;

		return $this;
	}

	/**
	 * Get the value of NOMCLUB
	 */ 
	public function getNOMCLUB()
	{
		return $this->NOMCLUB;
	}

	/**
	 * Set the value of NOMCLUB
	 *
	 * @return  self
	 */ 
	public function setNOMCLUB($NOMCLUB)
	{
		$this->NOMCLUB = $NOMCLUB;

		return $this;
	}

	/**
	 * Get the value of IDCOMMUNE
	 */ 
	public function getIDCOMMUNE()
	{
		return $this->IDCOMMUNE;
	}

	/**
	 * Set the value of IDCOMMUNE
	 *
	 * @return  self
	 */ 
	public function setIDCOMMUNE($IDCOMMUNE)
	{
		$this->IDCOMMUNE = $IDCOMMUNE;

		return $this;
	}

	/**
	 * Get the value of IDLIGUE
	 */ 
	public function getIDLIGUE()
	{
		return $this->IDLIGUE;
	}

	/**
	 * Set the value of IDLIGUE
	 *
	 * @return  self
	 */ 
	public function setIDLIGUE($IDLIGUE)
	{
		$this->IDLIGUE = $IDLIGUE;

		return $this;
	}

    /**
     * Get the value of IDCLUB
     */ 
    public function getIDCLUB()
    {
        return $this->IDCLUB;
    }

    /**
     * Set the value of IDCLUB
     *
     * @return  self
     */ 
    public function setIDCLUB($IDCLUB)
    {
        $this->IDCLUB = $IDCLUB;

        return $this;
    }
/*
    private ?string $idLigue;
    private ?int $idCommune;
    private ?string $nomClub;
    private ?string $adresseClub;


    public function getIdClub(): string
    {
        return $this->IDCLUB;
    }

    public function setIdClub(string $unIdClub): void
    {
        $this->IDCLUB = $unIdClub;
    }

    public function getIdLigue(): string
    {
        return $this->idLigue;
    }

    public function setIdLigue(string $unIdLigue): void
    {
        $this->idLigue = $unIdLigue;
    }

    public function getIdCommune(): string
    {
        return $this->idCommune;
    }

    public function setIdCommune(string $unIdCommune): void
    {
        $this->idCommune = $unIdCommune;
    }

    public function getNomClub(): string
    {
        return $this->nomClub;
    }

    public function setNomClub(string $unNomClub): void
    {
        $this->nomClub = $unNomClub;
    }

    public function getAdresseClub(): string
    {
        return $this->adresseClub;
    }

    public function setAdresseClub(string $uneAdresseClub): void
    {
        $this->adresseClub = $uneAdresseClub;
    }
*/
}