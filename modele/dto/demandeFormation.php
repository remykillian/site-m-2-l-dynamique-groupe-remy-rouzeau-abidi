<?php
class DemandeFormation{
    use hydrate;
	private ?int $IDFORMA;
	private ?int $IDUSER;
	private ?string $DEMANDE;

	public function __construct(?int $IDFORMA  , ?int $IDUSER , ?string $DEMANDE){
		$this->IDUSER = $IDUSER;
		$this->IDFORMA = $IDFORMA;
        $this->DEMANDE = $DEMANDE;

	
    }

	/**
	 * Get the value of IDFORMA
	 */ 
	public function getIDFORMA()
	{
		return $this->IDFORMA;
	}

	/**
	 * Set the value of IDFORMA
	 *
	 * @return  self
	 */ 
	public function setIDFORMA($IDFORMA)
	{
		$this->IDFORMA = $IDFORMA;

		return $this;
	}

	/**
	 * Get the value of IDUSER
	 */ 
	public function getIDUSER()
	{
		return $this->IDUSER;
	}

	/**
	 * Set the value of IDUSER
	 *
	 * @return  self
	 */ 
	public function setIDUSER($IDUSER)
	{
		$this->IDUSER = $IDUSER;

		return $this;
	}

	/**
	 * Get the value of DEMANDE
	 */ 
	public function getDEMANDE()
	{
		return $this->DEMANDE;
	}

	/**
	 * Set the value of DEMANDE
	 *
	 * @return  self
	 */ 
	public function setDEMANDE($DEMANDE)
	{
		$this->DEMANDE = $DEMANDE;

		return $this;
	}
}

    