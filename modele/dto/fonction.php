<?php
class Fonction{
    use Hydrate;
    private ?string $IDFONCT;
	private ?string $LIBELLE;

	public function __construct(?int $unIDFONCT){
	    $this->IDFONCT = $unIDFONCT;
    }
    /**
     * Get the value of IDFONCT
     */ 
    public function getIDFONCT()
    {
        return $this->IDFONCT;
    }

    /**
     * Set the value of IDFONCT
     *
     * @return  self
     */ 
    public function setIDFONCT($IDFONCT)
    {
        $this->IDFONCT = $IDFONCT;

        return $this;
    }

	/**
	 * Get the value of LIBELLE
	 */ 
	public function getLIBELLE()
	{
		return $this->LIBELLE;
	}

	/**
	 * Set the value of LIBELLE
	 *
	 * @return  self
	 */ 
	public function setLIBELLE($LIBELLE)
	{
		$this->LIBELLE = $LIBELLE;

		return $this;
	}
	}
