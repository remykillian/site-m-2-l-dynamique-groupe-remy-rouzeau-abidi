<?php
class Ligue{
    use Hydrate;
    private ?string $IDLIGUE;
	private ?string $NOMLIGUE;
	private ?string $SITE;
	private ?string $DESCRIPTIF;

	public function __construct(?int $unIDLIGUE){
	    $this->IDLIGUE = $unIDLIGUE;
	}

    /**
     * Get the value of IDLIGUE
     */ 
    public function getIDLIGUE()
    {
        return $this->IDLIGUE;
    }

    /**
     * Set the value of IDLIGUE
     *
     * @return  self
     */ 
    public function setIDLIGUE($IDLIGUE)
    {
        $this->IDLIGUE = $IDLIGUE;

        return $this;
    }

	/**
	 * Get the value of NOMLIGUE
	 */ 
	public function getNOMLIGUE()
	{
		return $this->NOMLIGUE;
	}

	/**
	 * Set the value of NOMLIGUE
	 *
	 * @return  self
	 */ 
	public function setNOMLIGUE($NOMLIGUE)
	{
		$this->NOMLIGUE = $NOMLIGUE;

		return $this;
	}

	/**
	 * Get the value of SITE
	 */ 
	public function getSITE()
	{
		return $this->SITE;
	}

	/**
	 * Set the value of SITE
	 *
	 * @return  self
	 */ 
	public function setSITE($SITE)
	{
		$this->SITE = $SITE;

		return $this;
	}

	/**
	 * Get the value of DESCRIPTIF
	 */ 
	public function getDESCRIPTIF()
	{
		return $this->DESCRIPTIF;
	}

	/**
	 * Set the value of DESCRIPTIF
	 *
	 * @return  self
	 */ 
	public function setDESCRIPTIF($DESCRIPTIF)
	{
		$this->DESCRIPTIF = $DESCRIPTIF;

		return $this;
	}

}