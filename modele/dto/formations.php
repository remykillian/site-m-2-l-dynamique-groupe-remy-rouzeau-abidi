<?php
class Formations{
	private array $formations ;

	public function __construct($array){
		if (is_array($array)) {
			$this->formations = $array;
		}
	}

	public function getFormation(){
		return $this->formations;
	}

	public function chercheFormation($unIDFormation){
		foreach ($this->formations as $formation) {
			if ($formation instanceof Formation && $unIDFormation == $formation->getIDFORMA()) {
				return $formation;
			}
		}
		return null;
	}
}