<?php
class Intervenants{
	private array $intervenants ;

	public function __construct($array){
		if (is_array($array)) {
			$this->intervenants = $array;
		}
	}

	public function getIntervenants(){
		return $this->intervenants;
	}

	public function chercheIntervenant($unIdIntervenant){
		foreach ($this->intervenants as $intervenant){
			if ($intervenant->getIdUser() == $unIdIntervenant){
				return $intervenant;
			}
		}
	}
}