<?php
class FormationDAO{

    public static function getAll(){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM `FORMATION`");
        
        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
     
        return $reponse;
    }

    public static function getByUser($userId){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM `PARTICIPE` WHERE IDUSER = :userId");
        
        $requetePrepa->bindParam( ":userId" , $userId);

        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
     
        return $reponse;
    }


    public static function lesFormations(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from FORMATION order by INTITULE" );
       
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($liste)){
            foreach($liste as $forma){
                $uneFormation = new Formation(null,null);
                $uneFormation->hydrate($forma);
                $result[] = $uneFormation;
            }
        }
        return $result;
    }


    public static function lesDemandesFormations(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from PARTICIPE" );
       
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($liste)){
            foreach($liste as $forma){
                $uneFormation = new DemandeFormation(null,null,null);
                $uneFormation->hydrate($forma);
                $result[] = $uneFormation;
            }
        }
        return $result;
    }




    public static function lesDemandesFormationsById($idForma){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from PARTICIPE WHERE IDFORMA = :id" );
       
        $requetePrepa->BindParam(":id", $idForma);

        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        return $liste;
    }

    public static function createFormation($intitule , $descriptif, $duree, $dateOuverture, $effectif){
        

        $requetePrepa2 = DBConnex::getInstance()->prepare("Select MAX(IDFORMA) FROM FORMATION");

        $requetePrepa2->execute();
        $liste = $requetePrepa2->fetchAll(PDO::FETCH_ASSOC);



        if(!empty($liste)){
            foreach($liste as $result){
                $maxId = $result;
            }
        }
        $maxId = $maxId['MAX(IDFORMA)'] + 1;

        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO `FORMATION`(`IDFORMA`, `INTITULE`, `DESCRIPTIF`, `DUREE`, `DATEOUVERTUREINSCRIPTION`, `EFFECTIF`) VALUES (:id , :intitule , :descriptif , :duree , :DateOuvertureInscription , :effectif )");

        $requetePrepa->BindParam(":id", $maxId);
        $requetePrepa->BindParam(":intitule", $intitule);
        $requetePrepa->BindParam(":descriptif", $descriptif);
        $requetePrepa->BindParam(":duree", $duree);
        $requetePrepa->BindParam(":DateOuvertureInscription", $dateOuverture);
        $requetePrepa->BindParam(":effectif", $effectif);


        $requetePrepa->execute();
        
        
    }



    public static function editFormation($id , $intitule , $descriptif , $duree , $dateOuverture , $dateCloture , $effectif){
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE `FORMATION` SET `INTITULE`= :intitule ,`DESCRIPTIF`= :descriptif ,`DUREE`= :duree ,`DATEOUVERTUREINSCRIPTION`= :dateOuverture,`DATECLOTUREINSCRIPTION`= :dateCloture,`EFFECTIF`= :effectif WHERE IDFORMA = :id");
        
        $requetePrepa->BindParam(":id", $id);
        $requetePrepa->BindParam(":intitule", $intitule);
        $requetePrepa->BindParam(":descriptif", $descriptif);
        $requetePrepa->BindParam(":duree", $duree);
        $requetePrepa->BindParam(":dateOuverture", $dateOuverture);
        $requetePrepa->BindParam(":dateCloture", $dateCloture);
        $requetePrepa->BindParam(":effectif", $effectif);

        $requetePrepa->execute();
    }

    public static function delFormation($id){
        $db = DBConnex::getInstance();
        $requetePrepa = $db->prepare("SELECT COUNT(*) FROM `PARTICIPE` WHERE IDFORMA = :id");
        $requetePrepa->bindParam(":id", $id);
        $requetePrepa->execute();
        $count = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
    
        if ($count > 0) {
            $deletePrepa= $db->prepare("DELETE FROM `PARTICIPE` WHERE IDFORMA = :id");
            $deletePrepa->bindParam(":id", $id);
            $deletePrepa->execute();
        } 
        $deleteFormaPrepa = $db->prepare("DELETE FROM `FORMATION` WHERE IDFORMA = :id");
        $deleteFormaPrepa->bindParam(":id", $id);
        $deleteFormaPrepa->execute();
        
    }

    public static function getFormationById($id){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("Select * From FORMATION WHERE IDFORMA = :id");

        $requetePrepa->bindParam(":id", $id);

        $requetePrepa->execute();

        $result = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getInscritByForma($idForma){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT UTILISATEUR.NOM , UTILISATEUR.PRENOM 
                                                            FROM `PARTICIPE` NATURAL JOIN UTILISATEUR
                                                            WHERE DEMANDE = 'Valider' AND PARTICIPE.IDFORMA = :id");

        $requetePrepa->bindParam(":id", $idForma);
        $requetePrepa->execute();
        $result = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }





}



?>