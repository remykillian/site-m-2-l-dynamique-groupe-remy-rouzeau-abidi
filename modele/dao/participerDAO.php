<?php
class ParticiperDAO{

    public static function getAll(){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM `PARTICIPE`");
        
        $requetePrepa->execute();

        $reponse = $requetePrepa->fetch(PDO::FETCH_ASSOC);
     
        return $reponse;
    }

    public static function getByUser($userId){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM `PARTICIPE` WHERE IDUSER = :userId");
        
        $requetePrepa->BindParam( ":userId" , $userId);

        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
     
        return $reponse;
    }

    public static function getByForma($formaId){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM `PARTICIPE` WHERE IDFORMA = :formaId");
        
        $requetePrepa->BindParam( ":formaId" , $formaId);

        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
     
        return $reponse;
    }

    public static function inscrireFormation($userId, $formaId){
        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO `PARTICIPE`(`IDFORMA`, `IDUSER`) VALUES (:idforma, :iduser)");
    
        $requetePrepa->bindParam(":idforma", $formaId);
        $requetePrepa->bindParam(":iduser", $userId);
    
        $requetePrepa->execute();
    }

    public static function getFormationInscritByUser($idUser){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT IDFORMA FROM `PARTICIPE` WHERE IDUSER = :userId");
        
        $requetePrepa->BindParam( ":userId" , $$idUser);

        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
     
        return $reponse;
    }

    public static function updateDemande($idForma , $idUser, $bool){
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE PARTICIPE SET DEMANDE= :bool WHERE IDFORMA = :idForma AND IDUSER = :idUser ");

        $valider = "Valider";
        $refuser = "Refuser";
        $requetePrepa->bindParam(":idForma" , $idForma);
        $requetePrepa->bindParam(":idUser" , $idUser);
        if($bool == 1){
            $requetePrepa->bindParam(":bool", $valider);
        }
        else{
            $requetePrepa->bindParam(":bool" , $refuser);
        }

        $requetePrepa->execute();
    }

    public static function delDemande($iduser){
        $requetePrepa = DBConnex::getInstance()->prepare("DELETE FROM PARTICIPE WHERE IDUSER = :idUser");

        $requetePrepa->bindParam(":idUser", $iduser);

        $requetePrepa->execute();
        
    }


    

}



?>