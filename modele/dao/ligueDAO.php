<?php
class ligueDAO{
       
    public static function lesLigues(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from LIGUE " ); 
       
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($liste)){
            foreach($liste as $ligue){
                $uneLigue = new Ligue(null,null);
                $uneLigue->hydrate($ligue);
                $result[] = $uneLigue;
            }
        }
        return $result;
    }


    public static function EnregistrerLigue(String $idLigue,String $nomLigue, String $site, String $descriptif){
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE `LIGUE` SET `NOMLIGUE`=:nomLigue,`SITE`=:site,`DESCRIPTIF`=:descriptif WHERE `LIGUE`.`IDLIGUE`=:idLigue");
        $requetePrepa->bindParam(":nomLigue",$nomLigue);
        $requetePrepa->bindParam(":site",$site);
        $requetePrepa->bindParam(":descriptif",$descriptif);
        $requetePrepa->bindParam(":idLigue",$idLigue);
        $requetePrepa->execute();
    }

    public static function AjoutLigue(String $idLigue,String $nomLigue, String $site, String $descriptif){
        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO LIGUE(idLigue , nomLigue , site, descriptif) VALUES (:idLigue,:nomLigue,:site,:descriptif)");
        $requetePrepa->bindParam(":idLigue",$idLigue);
        $requetePrepa->bindParam(":nomLigue",$nomLigue);
        $requetePrepa->bindParam(":site",$site);
        $requetePrepa->bindParam(":descriptif",$descriptif);
        $requetePrepa->execute();
    }

    public static function SupprLigue(String $idLigue){
        $requetePrepa = DBConnex::getInstance()->prepare("delete from LIGUE WHERE idLigue = :idLigue");
        $requetePrepa->bindParam( ":idLigue", $idLigue);
        $requetePrepa->execute();

    }


}
?>