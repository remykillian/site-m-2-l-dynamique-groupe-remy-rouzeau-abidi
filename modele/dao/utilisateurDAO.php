<?php
class UtilisateurDAO{
        
    public static function verification($unLogin, $unMdp){
        

        $requetePrepa = DBConnex::getInstance()->prepare("select IDUSER, TYPEUSER  from UTILISATEUR where LOGIN = :login and  MDP = :mdp");

        //$requetePrepa = DBConnex::getInstance()->prepare("select LOGIN, TYPEUSER  from UTILISATEUR where LOGIN = :login and  MDP = :mdp");

        $requetePrepa->bindParam( ":login", $unLogin);
        $requetePrepa->bindParam( ":mdp" ,  $unMdp);
        
        $requetePrepa->execute();

        $reponse = $requetePrepa->fetch(PDO::FETCH_ASSOC);
     
        return $reponse;
    }

 public static function getClubIntervenant($idClub){
    $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM CLUB WHERE IDCLUB = :idClub");
    $requetePrepa->bindParam( ":idClub", $idClub);
    $requetePrepa->execute();
    $reponse = $requetePrepa->fetch(PDO::FETCH_ASSOC);
    //var_dump($reponse);
    if(!empty($reponse)){
        $unClub = new Club(null);
        $unClub->hydrate($reponse);
    }
    return $unClub;
 }

 public static function getLigueIntervenant($idLigue){
    $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM LIGUE WHERE IDLIGUE = :idLigue");
    $requetePrepa->bindParam( ":idLigue", $idLigue);
    $requetePrepa->execute();
    $reponse = $requetePrepa->fetch(PDO::FETCH_ASSOC);
    //var_dump($reponse);
    if(!empty($reponse)){
            $uneLigue = new Ligue(null);
            $uneLigue->hydrate($reponse);
    }
    return $uneLigue;
 }

    public static function getIntervenant(){
        
        $requetePrepa = DBConnex::getInstance()->prepare("select  UTILISATEUR.IDUSER , UTILISATEUR.IDLIGUE, UTILISATEUR.IDCLUB,  UTILISATEUR.IDFONCT, UTILISATEUR.NOM, UTILISATEUR.PRENOM, UTILISATEUR.TYPEUSER, CLUB.NOMCLUB, LIGUE.NOMLIGUE, FONCTION.LIBELLE from UTILISATEUR, CLUB, LIGUE, FONCTION where UTILISATEUR.IDCLUB = CLUB.IDCLUB AND UTILISATEUR.IDLIGUE AND UTILISATEUR.IDFONCT = FONCTION.IDFONCT");
        
        //"select  UTILISATEUR.IDUSER , UTILISATEUR.IDLIGUE, UTILISATEUR.IDFONCT, UTILISATEUR.NOM, UTILISATEUR.PRENOM, UTILISATEUR.TYPEUSER, FONCTION.LIBELLE  (select NOMCLUB FROM CLUB ) from UTILISATEUR, FONCTION where UTILISATEUR.IDFONCT = FONCTION.IDFONCT" );
        

        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($reponse);
        $result = [];
        if(!empty($reponse)){
            foreach($reponse as $intervenant){
                $unIntervenant = new Intervenant(null);
                $unIntervenant->hydrate($intervenant);
                $unIntervenant->setClub(self::getClubIntervenant($unIntervenant->getIdClub()));
                $unIntervenant->setLigue(self::getLigueIntervenant($unIntervenant->getIdLigue()));
                $result[] = $unIntervenant;

            }
        }
        return $result;
    }    

    public static function getContrats($idUser){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from CONTRAT where idUser = :idUser");
        $requetePrepa->bindParam( ":idUser", $idUser);
        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        if(!empty($reponse)){
            foreach($reponse as $contrat){
                $unContrat = new Contrat(null);
                $unContrat->hydrate($contrat);
                $result[] = $unContrat;
            }
        }
        return $result;
    }    

    public static function createContrat($idUser,$dateDebut, $dateFin, $typeContrat,$nombreHeure){
        $requetePrepa = DBConnex::getInstance()->prepare("insert into CONTRAT VALUES(0, :idUser, :dateDebut, :dateFin, :typeContrat, :nombreHeure)");
        $requetePrepa->bindParam(":idUser", $idUser);
        $requetePrepa->bindParam(":dateDebut", $dateDebut);
        $requetePrepa->bindParam(":dateFin", $dateFin);
        $requetePrepa->bindParam(":typeContrat", $typeContrat);
        $requetePrepa->bindParam(":nombreHeure", $nombreHeure);

        $requetePrepa->execute();
        return DBConnex::getInstance()->lastInsertId(); //Faut changer auto-incrementation dans table
    }

    public static function updateContrat($idContrat,$dateDebut, $dateFin, $typeContrat,$nombreHeure){
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE CONTRAT SET dateDebut = :dateDebut, dateFin = :dateFin, typeContrat = :typeContrat, nbHeures = :nombreHeure WHERE idContrat = :idContrat");

        $requetePrepa->bindParam(":idContrat", $idContrat);
        $requetePrepa->bindParam(":dateDebut", $dateDebut);
        $requetePrepa->bindParam(":dateFin", $dateFin);
        $requetePrepa->bindParam(":typeContrat", $typeContrat);
        $requetePrepa->bindParam(":nombreHeure", $nombreHeure);

        $requetePrepa->execute();
    }


    public static function deleteContrat($idContrat){
        $requetePrepa = DBConnex::getInstance()->prepare("DELETE From CONTRAT WHERE idContrat = :idContrat");

        $requetePrepa->bindParam(":idContrat", $idContrat);

        $requetePrepa->execute();
    }


    public static function getBulletins($idContrat){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from BULLETIN where idContrat = :idContrat");
        $requetePrepa->bindParam( ":idContrat", $idContrat);
        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        if(!empty($reponse)){
            foreach($reponse as $bulletin){
                $unBulletin = new Bulletin(null);
                $unBulletin->hydrate($bulletin);
                $result[] = $unBulletin;
            }
        }
        return $result;
    }    

    public static function getFonctions(){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from FONCTION");
        $requetePrepa->execute();

        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        if(!empty($reponse)){
            foreach($reponse as $fonction){
                $uneFonction = new Fonction(null);
                $uneFonction->hydrate($fonction);
                $result[] = $uneFonction;
            }
        }
        return $result;
    }    

    


    public static function getIntervenants(){
        $requetePrepa = DBConnex::getInstance()->prepare("Select IDUSER, NOM, PRENOM from UTILISATEUR Where TYPEUSER = 'SAL' OR TYPEUSER = 'BEN'");


    //    $liste = [];

        //$SAL = "SAL";
        //$BEN = "BEN";

        //$requetePrepa->BindParam(":sal" , $SAL);
        //$requetePrepa->BindParam(":ben" , $BEN);

        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        //var_dump($liste);

        if(!empty($liste)){
            foreach($liste as $user){
                $unUser = new Utilisateur(null,null);
                $unUser->hydrate($user);
                $result[] = $unUser;
            }
        }
        return $result;
    }

    public static function getNomUserByID($idUser){
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT NOM , PRENOM FROM UTILISATEUR WHERE IDUSER = :id");

        $requetePrepa->bindParam(":id" , $idUser);

        $requetePrepa->execute();

        $result = $requetePrepa->fetch(PDO::FETCH_ASSOC);
        

        return $result;
    }
}
