<?php
class ClubDAO{
       
    public static function lesCLubs(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from CLUB" ); //"select CLUB.* from CLUB,LIGUE where LIGUE.IDLIGUE=CLUB.IDLIGUE"
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
        
        if(!empty($liste)){
            foreach($liste as $club){
                $unClub = new Club(null,null,null,null);
                $unClub->hydrate($club);
                $result[] = $unClub;
            }
        }
        return $result;
    }


    public static function AjoutClub(String $idClub,int $idLigue, int $idCommune, String $nomClub, String $adresseClub){
        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO `CLUB` (`IDCLUB`, `IDLIGUE`, `IDCOMMUNE`, `NOMCLUB`, `ADRESSECLUB`) VALUES (:idClub, :idLigue, :idCommune, :nomClub, :adresseClub)");
        $requetePrepa->bindParam(":idClub",$idClub);
        $requetePrepa->bindParam(":idLigue",$idLigue);
        $requetePrepa->bindParam(":idCommune",$idCommune);
        $requetePrepa->bindParam(":nomClub",$nomClub);
        $requetePrepa->bindParam(":adresseClub",$adresseClub);
        $requetePrepa->execute();
    }

    public static function SupprClub(String $idClub)
    {
        $requetePrepa = DBConnex::getInstance()->prepare("DELETE FROM CLUB WHERE `CLUB`.`IDCLUB` = :idClub ");
        $requetePrepa->bindParam(":idClub",$idClub);
        $requetePrepa->execute();
    }

    public static function EnregistrerClub(String $idClub, String $idLigue, int $idCommune,String $nomClub, String $adresseClub){
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE `CLUB` SET  `IDLIGUE` = :idLigue, `IDCOMMUNE` = :idCommune, `NOMCLUB` = :nomClub, `ADRESSECLUB` = :adresseClub WHERE `CLUB`.`IDCLUB` = :idClub");
        $requetePrepa->bindParam(":idLigue",$idLigue);
        $requetePrepa->bindParam(":idCommune",$idCommune);
        $requetePrepa->bindParam(":nomClub",$nomClub);
        $requetePrepa->bindParam(":adresseClub",$adresseClub);
        $requetePrepa->bindParam(":idClub",$idClub);
        $requetePrepa->execute();
    }
}
?>