<?php


$_SESSION['listeLigue'] = new ligues(ligueDAO::lesLigues());



if(isset($_GET['ligue'])){

    $_SESSION['ligue']= $_GET['ligue'];

}
else
{
    if(!isset($_SESSION['ligue'])){

        $_SESSION['ligue']=0;
    }

}


$_SESSION['ligueActive'] = $_SESSION['listeLigue']->chercheLigue($_SESSION['ligue']);


$menuLigue = new Menu("menuLigue");


foreach ($_SESSION['listeLigue']->getLigues() as $uneLigue){

    $menuLigue->ajouterComposant($menuLigue->creerItemLien($uneLigue->getNomLigue() ,$uneLigue->getIdLigue()));
}

$leMenuLigues = $menuLigue->creerMenuLigue($_SESSION['ligue']);





if($_SESSION['ligue']!=0){
    $formLigue =new Formulaire("post","index.php","formulaireLigue","formulaireLigue");


    $formLigue->ajouterComposantLigne($formLigue->creerLabel("Nom : " , "labelNom") , 1 );
    $formLigue->ajouterComposantLigne($formLigue->creerLabelFor("labelNomLigue", $_SESSION['ligueActive']->getNomLigue()) , 1 );
    $formLigue->ajouterComposantTab();
    $formLigue->ajouterComposantLigne($formLigue->creerLabel("Site  : " , "labelSite") , 1 );
    $formLigue->ajouterComposantLigne($formLigue->creerLinkFor("labelSiteLigue", $_SESSION['ligueActive']->getSite()) , 1 );
    $formLigue->ajouterComposantTab();
    $formLigue->ajouterComposantLigne($formLigue->creerLabel("Desciption : " , "labelDescriptif") , 1 );
    $formLigue->ajouterComposantLigne($formLigue->creerLabelFor( "labelDescriptif",$_SESSION['ligueActive']->getDescriptif()) , 1 );
    $formLigue->ajouterComposantTab();
    $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide(2));





    for($i=0;$i<count($_SESSION["club"]);$i++){
        if($_SESSION["club"][$i]->getIdLigue()==$_SESSION["ligueActive"]->getIdLigue()) {
            $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide());
            $formLigue->ajouterComposantLigne($formLigue->creerLabel("Club : ", "labelClub"), 1);
            $formLigue->ajouterComposantLigne($formLigue->creerLabelFor("labelClub", $_SESSION["club"][$i]->getNomClub()), 1);
            $formLigue->ajouterComposantTab();
            $formLigue->ajouterComposantLigne($formLigue->creerLabel("Adresse : " , "labelAdresse") , 1 );
            $formLigue->ajouterComposantLigne($formLigue->creerLabelFor( "labelAdresse",$_SESSION['club'][$i]->getAdresseClub()) , 1 );
            $formLigue->ajouterComposantTab();
        }
    }


    $formLigue->creerFormulaire();
}



require_once 'vue/ligues/vueLigues.php' ;
