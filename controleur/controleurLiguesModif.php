<?php


$_SESSION['listeLigue'] = new ligues(ligueDAO::lesLigues());



if(isset($_GET['ligue'])){

    $_SESSION['ligue']= $_GET['ligue'];

}
else
{
    if(!isset($_SESSION['ligue'])){

        $_SESSION['ligue']=0;
    }

}

$_SESSION['ligueActive'] = $_SESSION['listeLigue']->chercheLigue($_SESSION['ligue']);

if(isset($_POST['submitEnregistrer'])){
    ligueDAO::EnregistrerLigue($_SESSION['ligueActive']->getIdLigue(),$_POST['Nom'],$_POST['Site'],$_POST['Descriptif']);
}

if(isset($_POST['submitSupprDef'])){

    ligueDAO::SupprLigue($_SESSION['ligueActive']->getIdLigue());

}

if(isset($_POST['submitAjoutDef'])){
    //auto increment maison
    $ligues=ligueDAO::lesLigues();
    $maxi=$ligues[0]->getIdLigue();
    foreach ($ligues as $ligue){
        if((int)$maxi<(int)$ligue->getIdLigue()){
            $maxi=$ligue->getIdLigue();
        }
    }
    $maxi=(int)$maxi;
    $maxi++;
    $maxi=(String)$maxi;


    ligueDAO::AjoutLigue($maxi,$_POST['Nom'], $_POST['Site'], $_POST['Descriptif']);

}







$menuLigue = new Menu("menuLigue");


foreach ($_SESSION['listeLigue']->getLigues() as $uneLigue){

    $menuLigue->ajouterComposant($menuLigue->creerItemLien($uneLigue->getNomLigue() ,$uneLigue->getIdLigue()));
}

$leMenuLigues = $menuLigue->creerMenuLigue($_SESSION['ligue']);





if($_SESSION['ligue']!=0){

    $formLigue =new Formulaire("post","index.php","formulaireLigue","formulaireLigue");

    $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide());
    $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("SubmitSupprLigueTemp","btnSupprLigueTemp","supprimer la Ligue"));
    $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("SubmitAjoutLigueTemp","btnCreerLigue","creer une Ligue"));
    $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("SubmitModifLigue","btnModifLigue","modifier la Ligue"));
    $formLigue->ajouterComposantTab();
    $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide(2));



    if(isset($_POST["SubmitAjoutLigueTemp"]) ){
        $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Nom","nomNouvelleLigue","","1","Nom :","","0"));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Site","siteNouvelleLigue","","1","Site :","","0"));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Descriptif","descriptifNouvelleLigue","","1","Descriptif :","","0"));
        $formLigue->ajouterComposantTab();

        $_SESSION['ligueActive']=null;
        $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitAjoutDef', 'submitAjoutDef', 'Ajouter'));
        $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitAnnuler', 'submitAnnuler', 'Annuler'));
        $formLigue->ajouterComposantTab();
    }

    if(isset($_POST["SubmitModifLigue"])){
        $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Nom","nomNouvelleLigue",$_SESSION['ligueActive']->getNomLigue(),"1","Nom :","","0"));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Site","siteNouvelleLigue",$_SESSION['ligueActive']->getSite(),"1","Site :","","0"));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Descriptif","descriptifNouvelleLigue",$_SESSION['ligueActive']->getDescriptif(),"1","Descriptif :","","0"));
        $formLigue->ajouterComposantTab();

        $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitEnregistrer', 'submitEnregistrer', 'Enregistrer'));
        $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitAnnuler', 'submitAnnuler', 'Annuler'));
        $formLigue->ajouterComposantTab();
    }

    if(isset($_POST["SubmitSupprLigueTemp"])) {
        $formLigue->ajouterComposantLigne($formLigue->creerLabel("Vous etes sur que c'est ce que vous voulez ? " ));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitSupprDef', 'submitSupprDef', 'je sais ce que je fais'));
        $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitAnnuler', 'submitAnnuler', 'Annuler'));
        $formLigue->ajouterComposantTab();
        $_SESSION['ligueActive'] = $_SESSION['listeLigue']->chercheLigue("0");
    }

    if($_SESSION['ligueActive']!=null and !isset($_POST["SubmitModifLigue"])) {


        $formLigue->ajouterComposantLigne($formLigue->creerLabel("Nom : "));
        $formLigue->ajouterComposantLigne($formLigue->creerLabelFor("labelNomLigue", $_SESSION['ligueActive']->getNomLigue()));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerLabel("Site  : "));
        $formLigue->ajouterComposantLigne($formLigue->creerLinkFor("labelSiteLigue", $_SESSION['ligueActive']->getSite()));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerLabel("Desciption : "));
        $formLigue->ajouterComposantLigne($formLigue->creerLabelFor("labelDescriptif", $_SESSION['ligueActive']->getDescriptif()));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide(2));
    }

    /*****************************************************************************************************
     * GESTION DES CLUBS
     *****************************************************************************************************/




    if(isset($_POST['submitAjoutClubDef'])){

        //auto increment maison
        $clubs=ClubDAO::lesCLubs();
        $maxi=$clubs[0]->getIdClub();
        foreach ($clubs as $club){
            if((int)$maxi<(int)$club->getIdClub()){
                $maxi=$club->getIdClub();
            }
        }
        $maxi=(int)$maxi;
        $maxi++;
        $maxi=(String)$maxi;


        ClubDAO::AjoutClub($maxi,$_SESSION['ligueActive']->getIdLigue(),(int)$_POST['Commune'],$_POST['LabelClub'], $_POST['Adresse']);

    }


    if($_SESSION['ligueActive']!=null) {
        $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("SubmitAjoutClubTemp","btnCreerClub","creer un club"));
        $formLigue->ajouterComposantTab();
        $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide(2));

        if(isset($_POST["SubmitAjoutClubTemp"]) ){

            $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("LabelClub","labelNouveauClub","","1","Nom :","","0"));
            $formLigue->ajouterComposantTab();
            $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Adresse","adresseNouveauClub","","1","Site :","","0"));
            $formLigue->ajouterComposantTab();
            $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Commune","communeNouveauClub","","1","Commune :","","0"));
            $formLigue->ajouterComposantTab();

            $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitAjoutClubDef', 'submitAjoutClubDef', 'Ajouter'));
            $formLigue->ajouterComposantLigne($formLigue-> creerInputSubmit('submitAnnuler', 'submitAnnuler', 'Annuler'));
            $formLigue->ajouterComposantTab();
            $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide(2));

        }

        for ($i = 0; $i < count($_SESSION["club"]); $i++) {


            if ($_SESSION["club"][$i]->getIdLigue() == $_SESSION["ligueActive"]->getIdLigue()) {

                $formLigue->ajouterComposantLigne($formLigue->creerEspaceVide());
                $formLigue->ajouterComposantLigne($formLigue->creerLabel("Club : "));
                $formLigue->ajouterComposantLigne($formLigue->creerLabelFor("labelClub", $_SESSION["club"][$i]->getNomClub()));
                $formLigue->ajouterComposantTab();
                $formLigue->ajouterComposantLigne($formLigue->creerLabel("Adresse : "));
                $formLigue->ajouterComposantLigne($formLigue->creerLabelFor("labelAdresse", $_SESSION['club'][$i]->getAdresseClub()));
                $formLigue->ajouterComposantTab();

                $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("SubmitSupprClub".$_SESSION["club"][$i]->getNomClub(),"btnSupprClub","supprimer le club"));
                $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("SubmitModifClub".$_SESSION["club"][$i]->getNomClub(),"btnModifClub","modifier le club"));
                $formLigue->ajouterComposantTab();

                if(isset($_POST["SubmitSupprClub".$_SESSION["club"][$i]->getNomClub()]) ){
                    ClubDAO::SupprClub($_SESSION["club"][$i]->getIdClub());
                }

                if(isset($_POST["SubmitModifClub".$_SESSION["club"][$i]->getNomClub()]) ) {
                    $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Label","labelClub",$_SESSION["club"][$i]->getNomClub(),"1","Label :","","0"));
                    $formLigue->ajouterComposantTab();
                    $formLigue->ajouterComposantLigne($formLigue->creerInputTexte("Adresse","adresseClub",$_SESSION["club"][$i]->getAdresseClub(),"1","Adresse :","","0"));
                    $formLigue->ajouterComposantTab();


                    $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit("submitEnregistrerClub","btnValider","valider"));
                    $formLigue->ajouterComposantLigne($formLigue->creerInputSubmit('submitAnnuler', 'submitAnnuler', 'Annuler'),2);
                    $formLigue->ajouterComposantTab();
                }

                if(isset($_POST['submitEnregistrerClub'])){
                    ClubDAO::EnregistrerClub($_SESSION["club"][$i]->getIdClub(),$_SESSION["club"][$i]->getIdLigue(),$_SESSION["club"][$i]->getIdCommune(),$_POST['Label'],$_POST['Adresse']);
                }
            }
        }


    }


    $formLigue->creerFormulaire();
}



require_once 'vue/ligues/vueLigues.php' ;
