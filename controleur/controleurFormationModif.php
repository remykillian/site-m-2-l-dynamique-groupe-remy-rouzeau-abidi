<?php

if(isset($_GET['formation'])){
	$_SESSION['formation']= $_GET['formation'];
}
else
{
	if(!isset($_SESSION['formation'])){
		$_SESSION['formation']="0";
	}
}

$_SESSION['ajouter'] = 0;
$menuFormation = new Menu("menuFormation");
$menuFormationPasse = new Menu("menuFormationPasse");
$formInfo = new Formulaire("post","index.php","formuForma","formuForma");

$_SESSION['listeFormation'] = new Formations(FormationDAO::lesFormations());

foreach ($_SESSION['listeFormation']->getFormation() as $uneFormation){
	$idForma = (string) $uneFormation->getIDFORMA();
	if(strtotime($uneFormation->getDATECLOTUREINSCRIPTION()) != null){
		if(strtotime($uneFormation->getDATECLOTUREINSCRIPTION()) > strtotime(date('Y-m-d'))){
			$menuFormation->ajouterComposant($menuFormation->creerItemLien($idForma , $uneFormation->getINTITULE()));
		}
		else{
			$menuFormationPasse->ajouterComposant($menuFormationPasse->creerItemLien($idForma , $uneFormation->getINTITULE()));
		}
	}
	else{
		$menuFormation->ajouterComposant($menuFormation->creerItemLien($idForma , $uneFormation->getINTITULE()));
	}
	

	
}

$leMenuFormationPasse = $menuFormationPasse->creerMenu($_SESSION['listeFormation'], "formation");

$leMenuFormation = $menuFormation->creerMenu($_SESSION['listeFormation'], "formation");


$_SESSION['FormationActive'] = $_SESSION['listeFormation']->chercheFormation($_SESSION['formation']);

if(isset($_POST['Supprimer'])){
	$var = new FormationDAO;
	$id = $_SESSION['FormationActive']->getIDFORMA();
	$var->delFormation($id);

}

if(isset($_POST['Creer'])){
	
	$_SESSION['ajouter'] = 1;
	$_SESSION['formation'] = null;

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Intitule Formation : " , "labelFormation") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("intituleFormation", "intituleFormation", "" , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Descriptif : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("descriptifFormation", "descriptifFormation", "" , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Duree (en minutes) : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dureeFormation", "dureeFormation", "" , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date ouverture Inscription : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("ouverture", "ouverture", "" , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Effectif : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("effectif", "effectif", "" , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit("Annuler","Annuler","Annuler"),1);
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit("VAjouter","VAjouter","Ajouter"),1);
	$formInfo->ajouterComposantTab();

	$formInfo->creerFormulaire();

}

if(isset($_POST['VAjouter'])){
	$nomFormation = $_POST['intituleFormation'];
	$descriptif = $_POST['descriptifFormation'];
	$duree = $_POST['dureeFormation'];
	$dateOuverture = $_POST['ouverture'];
	$effectif = $_POST['effectif'];

	$var = new FormationDAO;
	$var->createFormation($nomFormation, $descriptif, $duree, $dateOuverture, $effectif);
	$_SESSION['ajouter'] = 0;

}


if(isset($_POST['Modifier'])){
	
	$_SESSION['ajouter'] = 1;
	$_SESSION['formation'] = null;

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Intitule Formation : " , "labelFormation") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("intituleFormation", "intituleFormation", $_SESSION['FormationActive']->getINTITULE() , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Descriptif : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("descriptifFormation", "descriptifFormation", $_SESSION['FormationActive']->getDESCRIPTIF() , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Duree (en minutes) : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dureeFormation", "dureeFormation", $_SESSION['FormationActive']->getDUREE() , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date ouverture Inscription : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("ouverture", "ouverture", $_SESSION['FormationActive']->getDATEOUVERTUREINSCRIPTION() , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date cloture Inscription : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("cloture", "cloture", "" , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerLabel("Effectif : " , "labelEquipe") , 1 );
	$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("effectif", "effectif", $_SESSION['FormationActive']->getEFFECTIF() , "1" , "",  "0", "0") , 2 );
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit("Annuler","Annuler","Annuler"),1);
	$formInfo->ajouterComposantTab();

	$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit("VEnregistrer","VEnregistrer","Enregistrer"),1);
	$formInfo->ajouterComposantTab();

	$formInfo->creerFormulaire();

}

if(isset($_POST['VEnregistrer'])){
	$id = $_SESSION['FormationActive']->getIDFORMA();
	$nomFormation = $_POST['intituleFormation'];
	$descriptif = $_POST['descriptifFormation'];
	$duree = $_POST['dureeFormation'];
	$dateOuverture = $_POST['ouverture'];
	$dateCloture = $_POST['cloture'];
	$effectif = $_POST['effectif'];

	$var = new FormationDAO;
	$var->EditFormation($id , $nomFormation, $descriptif, $duree, $dateOuverture, $dateCloture ,$effectif);
	$_SESSION['ajouter'] = 0;

}


if(isset($_POST['VoirDemande'])){


	$formDemande = new Formulaire("post" , "index.php" , "formDemande" , "formDemande");
	$listeDemande = ParticiperDAO::getByForma($_SESSION['FormationActive']->getIDFORMA());

	$formDemande->ajouterComposantLigne($formDemande->creerLabel("Demande d'inscription : " , "Demande"));
	$formDemande->ajouterComposantTab();

	if(empty($listeDemande) || $listeDemande == null){
		$formDemande->ajouterComposantLigne($formDemande->creerLabel("Pas de demande d'inscription" , "Demande"));//Si il n'y a pas de demande ou d'inscrit pour cette formation
		$formDemande->ajouterComposantTab();
	}
	else{

		foreach ($listeDemande as $uneDemande){
			$etatDemande =$uneDemande["DEMANDE"];
			if($etatDemande == "En Attente"){
				$idUser = $uneDemande["IDUSER"];
				$user = UtilisateurDAO::getNomUserByID($idUser);
				$nomPrenom = $user["NOM"]. " " .$user["PRENOM"];
				$idForma = (string) $uneDemande["IDFORMA"];
				$_SESSION['idForma'] = $idForma;
				$formDemande->ajouterComposantLigne($formDemande->creerInputSecret("id", "id" , $idUser, "" ,"", "", "1"), 1);
				$formDemande->ajouterComposantLigne($formDemande->creerInputTexte("nomPrenom" , "nomPrenom", $nomPrenom , "1", "", "", "1"), 1);
				$formDemande->ajouterComposantLigne($formDemande->creerInputSubmit("Accepter" , "Accepter" , "Accepter"), 1);
				$formDemande->ajouterComposantLigne($formDemande->creerInputSubmit("Refuser" , "Refuser" , "Refuser"), 1);
				$formDemande->ajouterComposantTab();
			}
			else{
				$formDemande->ajouterComposantLigne($formDemande->creerLabel("Pas de demande d'inscription" , "Demande"));// Si il y a des inscrit pour cette Formation mais pas de demande
				$formDemande->ajouterComposantTab();
			}

		}
		
	}
	$formDemande->creerFormulaire();
}

if(isset($_POST['Accepter'])){
	$idUser = $_POST["id"];
	$var = new ParticiperDAO;
	$idForma = $_SESSION['idForma'];
	$var->updateDemande($idForma, $idUser, 1);
}

if(isset($_POST['Refuser'])){
	$var = new ParticiperDAO;
	$idUser = $_POST["id"];
	$idForma = $_SESSION['idForma'];
	$var->updateDemande($idForma, $idUser , 0);
}

if(isset($_POST['VoirInscrit'])){
	echo "aaa";
	$var = new FormationDAO;
	$results = $var->getInscritByForma($_SESSION['FormationActive']->getIDFORMA());
	$formInscrit = new Formulaire("post" , "index.php" , "formDemande" , "formDemande");

	if($results != null){
		$formInscrit->ajouterComposantLigne($formInscrit->creerLabel("Inscrit :", "inscrit"));
		$formInscrit->ajouterComposantTab();
		foreach($results as $result){
			$formInscrit->ajouterComposantLigne($formInscrit->creerLabel($result['NOM']." ".$result['PRENOM'], "inscrit"));
			$formInscrit->ajouterComposantTab();
		}
		
	}
	else{
		$formInscrit->ajouterComposantLigne($formInscrit->creerLabel("Il n'y a pas d'inscrit pour cette formation", "inscrit"));
		$formInscrit->ajouterComposantTab();
	}

	$formInscrit->creerFormulaire();
}


if($_SESSION['formation'] != null){
	if($_SESSION['FormationActive'] != null){
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Intitule Formation : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("intituleFormation", "intituleFormation", $_SESSION['FormationActive']->getINTITULE() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Descriptif Formation : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("descriptifFormation", "descriptifFormation", $_SESSION['FormationActive']->getDESCRIPTIF() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Durée Formation (en min) : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dureeFormation", "dureeFormation", $_SESSION['FormationActive']->getDUREE() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date Ouverture inscription : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dateOuverture", "dateOuverture", $_SESSION['FormationActive']->getDATEOUVERTUREINSCRIPTION() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date Fermeture inscription : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dateFermeture", "dateFermeture", $_SESSION['FormationActive']->getDATECLOTUREINSCRIPTION() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Effectif : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("effectif", "effectif", $_SESSION['FormationActive']->getEFFECTIF() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('Creer', 'creer', 'Créer une formation') , 1);
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('Modifier', 'modifier', 'Modifier une formation') , 1);
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('Supprimer', 'supprimer', 'Supprimer une formation') , 1);
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('VoirDemande', 'VoirDemande', 'Voir demande inscription') , 1);
		$formInfo->ajouterComposantTab();

		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('VoirInscrit', 'VoirInscrit', 'Voir inscrit') , 1);
		$formInfo->ajouterComposantTab();
	}
	else{
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Sélectionnez une formation", "labelFormation"));
		$formInfo->ajouterComposantTab();
	}

}
elseif($_SESSION['formation'] == null || $_SESSION['ajouter'] == 1){
	$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('Creer', 'creer', 'Créer une formation') , 1);
	$formInfo->ajouterComposantTab();
}
$formInfo->creerFormulaire();

require_once 'vue/vueFormation.php' ;
