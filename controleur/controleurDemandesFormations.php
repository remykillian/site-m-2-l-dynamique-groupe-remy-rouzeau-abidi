<?php
if(isset($_GET['intervenant'])){
	$_SESSION['intervenant']= $_GET['intervenant'];
}
else
{
	if(!isset($_SESSION['intervenant'])){
		$_SESSION['intervenant']="0";
	}
}

$menuIntervenant = new Menu("menuIntervenant");

$formInfo = new Formulaire("post","index.php","formuForma","formuForma");
$_SESSION['listeIntervenant'] = new Utilisateurs(utilisateurDAO::getIntervenants());

foreach ($_SESSION['listeIntervenant']->getUtilisateur() as $unUser){
	$idUser = (string) $unUser->getIDUSER();
    $nomPrenom = $unUser->getNOM(). " " . $unUser->getPRENOM();
	$menuIntervenant->ajouterComposant($menuIntervenant->creerItemLien($idUser , $nomPrenom));
}

$leMenuIntervenant = $menuIntervenant->creerMenu($_SESSION['listeIntervenant'], "intervenant");

$_SESSION['intervenantActif'] = $_SESSION['listeIntervenant']->chercheUtilisateur($_SESSION['intervenant']);
$_SESSION['intervenantActifInscription'] = new DemandesFormations(ParticiperDAO::getByUser($_SESSION['intervenantActif']->getIDUSER));

if($_SESSION['intervenant'] != null){

	foreach($_SESSION['intervenantActifInscription']->getDemandesFormation() as $uneInscription){
		$uneFormation = FormationDAO::getFormationById($uneInscription->getIDFORMA());
		var_dump($uneFormation);
		$formInfo->ajouterComposantLigne($formInfo->creerLabel($uneFormation['INTITULEE']) , 1);
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit("Accepter", "Accepter", "Accepter") , 1);
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit("Refuser", "Refuser", "Refuser") , 1);
		$formInfo->ajouterComposantTab();
	}


}
elseif($_SESSION['intervenant'] == null || $_SESSION['ajouter'] == 1){

}
$formInfo->creerFormulaire();

require_once 'vue/vueIntervenantRF.php' ;