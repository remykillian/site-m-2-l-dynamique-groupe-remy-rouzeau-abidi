<?php
$messageErreurConnexion = "";


//test à remplacer au plus vite
$_SESSION['club']=ClubDAO::lesClubs();

if(!isset($_SESSION['authentification'])){
	$_SESSION['authentification']=false;
}


if(isset($_GET['m2lMP'])){
	$_SESSION['m2lMP']= $_GET['m2lMP'];
}
else
{
	if(!isset($_SESSION['m2lMP'])){
		$_SESSION['m2lMP']="accueil";
	}
}


//Tester la connexion 
if(isset($_POST['login'])){
	$auth = UtilisateurDAO::verification($_POST['login'], $_POST['mdp']);
	//var_dump($auth["TYPEUSER"]);
	if($auth == null){
		$messageErreurConnexion = "Login ou mot de passe incorrect";
	}
	else{
		$_SESSION['idUser'] = $auth["IDUSER"];
		$_SESSION['authentification'] = $auth["TYPEUSER"];
		$_SESSION['m2lMP']="accueil";
	}
}


$m2lMP = new Menu("m2lMP");

$m2lMP->ajouterComposant($m2lMP->creerItemLien("accueil", "Accueil"));
$m2lMP->ajouterComposant($m2lMP->creerItemLien("services", "Services"));
$m2lMP->ajouterComposant($m2lMP->creerItemLien("locaux", "Locaux"));

$m2lMP->ajouterComposant($m2lMP->creerItemLien("ligues", "ligues"));

if($_SESSION['authentification'] == false){
	$m2lMP->ajouterComposant($m2lMP->creerItemLien("connexion", "Se connecter"));


}
else{
	

	if($_SESSION['authentification'] == 'SEC'){
	
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("liguesModif", "Editions ligues et Clubs"));
	
	}
	elseif($_SESSION['authentification'] == 'SAL'){
	
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("contrat", "Contrats et Bulletins"));
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("formation", "Formations"));

	}
	elseif($_SESSION['authentification'] == 'BEN'){
	
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("formation", "Formations"));

	}
	elseif($_SESSION['authentification'] == 'RH'){
	
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("intervenant", "Intervenants"));
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("fonction", "Fonctions"));
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("contratModif", "Gestions Contrats"));
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("bulletins", "Bulletins de Salaire"));

	}
	elseif($_SESSION['authentification'] == 'RF'){
	
		$m2lMP->ajouterComposant($m2lMP->creerItemLien("formationModif", "Editions Formations"));
	
	}

	$m2lMP->ajouterComposant($m2lMP->creerItemLien("connexion", "Se deconnecter"));
}


$menuPrincipalM2L = $m2lMP->creerMenu($_SESSION['m2lMP'],'m2lMP');

include_once dispatcher::dispatch($_SESSION['m2lMP']);




