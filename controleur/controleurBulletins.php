<?php

$lesIntervenants = new Intervenants(UtilisateurDAO::getIntervenants());

$menuIntervenants = new Menu("menuIntervenants");


foreach ($lesIntervenants->getIntervenants() as $intervenant){
    $menuIntervenants->ajouterComposant($menuIntervenants->creerLienIntervenants($intervenant->getNom()." ".$intervenant->getPrenom() ,$intervenant->getIdUser()));
}

if(isset($_GET['intervenant'])){
	$_SESSION['intervenant'] = $_GET['intervenant'];
}
else
{
	if(!isset($_SESSION['intervenant'])){
		$_SESSION['intervenant']="0";
	}
}

$_SESSION['modifier'] = "0";
$_SESSION['contratChoisis']="0";

if (isset($_GET['actionContrat'])){
    if ($_GET['actionContrat'] == "Ajouter"){

    }
    elseif ($_GET['actionContrat'] == "Modifier"){
        $_SESSION['modifier'] = $_SESSION['intervenant'];
    }
    elseif ($_GET['actionContrat'] == "Supprimer"){
         
    }
}

if (isset($_GET['actionContrat2'])){
    if ($_GET['actionContrat2'] == "Ajouter"){
        $reponseSGBD  = UtilisateurDAO::createContrat($_SESSION['intervenant'],$_POST['datedebut'], $_POST['datefin'], $_POST['typecontrat'],$_POST['nombreheure']);
    }
    elseif ($_GET['actionContrat2'] == "Modifier"){
        $reponseSGBD  = UtilisateurDAO::updateContrat($_POST['numerocontrat'],$_POST['datedebut'], $_POST['datefin'], $_POST['typecontrat'],$_POST['nombreheure']);
    }
    elseif ($_GET['actionContrat2'] == "Supprimer"){
        $reponseSGBD  = UtilisateurDAO::deleteContrat($_POST['numerocontrat']);
    }
}

if (isset($_POST['contratChoisis'])) {
    $_SESSION['contratChoisis'] = $_POST['contratChoisis'];
}

$leMenuIntervenants = $menuIntervenants->creerMenuContrat($_SESSION['intervenant']);

$listeContrats = UtilisateurDAO::getContrats($_SESSION['intervenant']);


function estDateDepassee($dateEnString) {
    $date = DateTime::createFromFormat('d/m/Y', $dateEnString);
    if ($date !== false) {
        $dateActuelle = new DateTime();
        if ($date < $dateActuelle) {
            return true; 
        } else {
            return false; 
        }
    } else {
        return false;
    }
}


if($_SESSION['intervenant'] != "0" && !isset($_GET['actionContrat']) ) {
    $formulaireContrat = new Formulaire('post', 'index.php', 'fContrat', 'fContrat');
	$listeContrats = UtilisateurDAO::getContrats($_SESSION['intervenant']);
    if (!empty($listeContrats)){
        foreach ($listeContrats as $Contrat){
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerLabel("Contrat n°: ".$Contrat->getIDCONTRAT()." ", "labelContrat"));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->ajouterRadio($Contrat->getIDCONTRAT()));
            $formulaireContrat->ajouterComposantTab();
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerLabel('Type:'));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputTexte("nomContrat", "nomContrat", $Contrat->getTYPECONTRAT(), "", "", "",1));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerLabel('Nombre heures:'));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputTexte("nomContrat", "nomContrat", $Contrat->getNBHEURES(), "", "", "",1));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerLabel('Date Debut:'));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputTexte("nomContrat", "nomContrat", $Contrat->getDATEDEBUT(), "", "", "",1));
            $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerLabel('Actif:'));
            if (estDateDepassee($Contrat->getDATEFIN())){
                $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputTexte("nomContrat", "nomContrat", "Non", "", "", "",1));
                $formulaireContrat->ajouterComposantTab();
            }else{
                $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputTexte("nomContrat", "nomContrat", "Oui", "", "", "",1));
                $formulaireContrat->ajouterComposantTab();
            }
        }
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputSubmit("", "", ""));
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputSubmit("actionContrat", "actionContrat", "Ajouter"));
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputSubmit("actionContrat", "actionContrat", "Modifier"));
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputSubmit("actionContrat", "actionContrat", "Supprimer"));
        $formulaireContrat->ajouterComposantTab();
    }
    else{
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerLabel('Aucun Contrat'));
        $formulaireContrat->ajouterComposantTab();
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputSubmit("", "", ""));
        $formulaireContrat->ajouterComposantLigne($formulaireContrat->creerInputSubmit("actionContrat", "actionContrat", "Ajouter"));
        $formulaireContrat->ajouterComposantTab();
    }
    $formulaireContrat->creerFormulaire();
}
else{
    $formulaireContrat = new Formulaire('post', 'index.php', 'fContrat', 'fContrat');
    $formulaireContrat->creerFormulaire();
}




require_once 'vue/vueBulletins.php';

