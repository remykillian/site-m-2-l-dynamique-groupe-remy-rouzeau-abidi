<?php

$mesDemandes = new Formulaire("post", "index.php", "demandes", "demandes");


$mesDemandes->ajouterComposantLigne($mesDemandes->creerInputSubmit("mesDemandes", "submit", "Mes Demandes"));
$mesDemandes->ajouterComposantTab();


$mesDemandes->creerFormulaire();

if(isset($_GET['formation'])){
	$_SESSION['formation']= $_GET['formation'];
}
else
{
	if(!isset($_SESSION['formation'])){
		$_SESSION['formation']="0";
	}
}

$menuFormation = new Menu("menuFormation");
$menuFormationPasse = new Menu("menuFormationPasse");
$formInfo = new Formulaire("post","index.php","formuForma","formuForma");

$_SESSION['listeFormation'] = new Formations(FormationDAO::lesFormations());
$idUser = $_SESSION['idUser'];

$demande = new DemandesFormations(ParticiperDAO::getByUser($idUser));

$results = ParticiperDAO::getByUser($idUser);
$idFormas = [];
foreach($results as $result){
	$idFormas[] = $result['IDFORMA'];
}
foreach ($_SESSION['listeFormation']->getFormation() as $uneFormation) {
    $idForma = (string) $uneFormation->getIDFORMA();

    if (strtotime($uneFormation->getDATECLOTUREINSCRIPTION()) != null) {
        if (strtotime($uneFormation->getDATECLOTUREINSCRIPTION()) > strtotime(date('Y-m-d'))) {
            foreach ($demande->getDemandesFormation() as $uneInscription) {

                if (is_array($uneInscription) && $uneInscription['IDFORMA'] == $idForma) {
                    $menuFormationPasse->ajouterComposant($menuFormationPasse->creerItemLien($idForma, $uneFormation->getINTITULE()));
                }
            }
        }
    } else {
        if (!in_array($idForma, $idFormas)) {
            $menuFormation->ajouterComposant($menuFormation->creerItemLien($idForma, $uneFormation->getINTITULE()));
        }
    }
}
$leMenuFormationPasse = null;// $menuFormationPasse->creerMenu($_SESSION['listeFormation'], "formation");

$leMenuFormation = $menuFormation->creerMenu($_SESSION['listeFormation'], "formation");


$_SESSION['FormationActive'] = $_SESSION['listeFormation']->chercheFormation($_SESSION['formation']);




if($_SESSION['formation'] != null){
	if($_SESSION['FormationActive'] != null){
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Intitule Formation : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("intituleFormation", "intituleFormation", $_SESSION['FormationActive']->getINTITULE() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Descriptif Formation : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("descriptifFormation", "descriptifFormation", $_SESSION['FormationActive']->getDESCRIPTIF() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Durée Formation (en min) : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dureeFormation", "dureeFormation", $_SESSION['FormationActive']->getDUREE() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date Ouverture inscription : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dateOuverture", "dateOuverture", $_SESSION['FormationActive']->getDATEOUVERTUREINSCRIPTION() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Date Fermeture inscription : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("dateFermeture", "dateFermeture", $_SESSION['FormationActive']->getDATECLOTUREINSCRIPTION() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerLabel("Effectif : " , "labelFormation") , 1 );
		$formInfo->ajouterComposantLigne($formInfo->creerInputTexte("effectif", "effectif", $_SESSION['FormationActive']->getEFFECTIF() , "0" , "",  "1", "1") , 1 );
		$formInfo->ajouterComposantTab();
	
		$formInfo->ajouterComposantLigne($formInfo->creerInputSubmit('inscription', 'inscription', 'Inscription') , 1);
		$formInfo->ajouterComposantTab();
	}






}

$formInfo->creerFormulaire();


if(isset($_POST['inscription'])){
	$idForma = $_SESSION['FormationActive']->getIDFORMA();
	$idUser = $_SESSION['idUser'];

	$var = new ParticiperDAO;
	$var->inscrireFormation($idUser , $idForma);


}

if(isset($_POST['mesDemandes'])){
	$results = ParticiperDAO::getByUser($idUser);
	$formMesDemande = new Formulaire('post', 'index.php' , 'formDemande' , 'formDemande');
	$formation = new FormationDAO;
	foreach($results as $result){
		$forma = $formation->getFormationById($result['IDFORMA']);
		$formMesDemande->ajouterComposantLigne($formMesDemande->creerLabel($forma[0]['INTITULE']." " ,"formationI"));
		$formMesDemande->ajouterComposantLigne($formMesDemande->creerLabel($result['DEMANDE']." ","formationD"));
		$formMesDemande->ajouterComposantLigne($formMesDemande->creerInputSubmit("SupprimerDem", "SupprimerDem", "Annuler"));
		$formMesDemande->ajouterComposantTab();
	}

	$formMesDemande->creerFormulaire();

}

if(isset($_POST['SupprimerDem'])){
	ParticiperDAO::delDemande($idUser);
}

require_once 'vue/vueFormation.php' ;
