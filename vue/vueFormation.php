<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
		<div class="forma">
			<aside>
				<div class="formFormaGauche">
					<?php  include 'vueFormationGauche.php' ;?> 
				</div>
			</aside>
			<aside>
				<div class="formForma">
					<?php $formInfo->afficherFormulaire(); ?>
				</div>
			</aside>
			<aside>
				<div class="formFormaDroite">
					<?php include 'vueFormationDroite.php' ;?>
				</div>
			</aside>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>