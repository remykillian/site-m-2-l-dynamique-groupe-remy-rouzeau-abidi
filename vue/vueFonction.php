<div class="conteneur">
    <header>
        <?php include 'haut.php' ;?>
    </header>
    <main>
        <?php 
        foreach ($lesFonctions as $fonction) { 
            echo "<div class='table-container'>"; 
        ?>
        <div class='listeIntervenant'>
            <h1><span>Intevenants avec la fonction: <?php echo $fonction->getLIBELLE(); ?></span></h1>
            <table class="intervenants-table">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Club</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach ($lesIntervenants->getIntervenants() as $intervenant) { 
                        if ($intervenant->getLIBELLE() == $fonction->getLIBELLE()) { 
                    ?>	 
                    <tr>
                        <td><?php echo $intervenant->getNom() ?></td>
                        <td><?php echo $intervenant->getPrenom() ?></td>
                        <td><?php echo $intervenant->getClub()->getNOMCLUB() ?></td>
                    </tr>
                    <?php } 
                    } 
                    ?>	
                </tbody>
            </table>
        </div>
        <?php 
            echo "</div>"; 
        }
        ?>
    </main>
    <footer>
        <?php include 'bas.php' ;?>
    </footer>
</div>