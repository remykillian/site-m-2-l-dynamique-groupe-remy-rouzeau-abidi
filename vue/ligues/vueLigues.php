<div class="conteneur">
	<header>
		<?php include 'vue/haut.php' ;?>
	</header>
	<main <?php if($_SESSION['authentification'] == 'SEC') { echo 'style="margin-top: 250px;"'; } ?>>
		<div class="pageLigue">
			<div class="menuDesLigues">
				<?php include 'vue/ligues/vueLiguesGauche.php';?> 
			</div>
			<div class="contenuDesLigues">
				<?php 
				if($_SESSION['ligue']!=0){
					$formLigue->afficherFormulaire();
				}
				?> 
			</div>
		</div> 

	</main>
	<footer>
		<?php include 'vue/bas.php' ;?>
	</footer>
</div>