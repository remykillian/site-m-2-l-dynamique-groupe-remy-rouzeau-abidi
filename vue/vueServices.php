<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main <?php if($_SESSION['authentification'] == 'SEC') { echo 'style="margin-top: 250px;"'; } ?>>
		<div class='gauche'>
			<?php include 'vue/services/servicesGauche.php' ;?>
		</div>
		<div class='droite'>
			<?php include 'vue/services/servicesDroite.php' ;?>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>