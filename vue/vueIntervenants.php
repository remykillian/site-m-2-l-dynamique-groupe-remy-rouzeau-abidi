<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
		<div class='listeIntervenant'>
			<h1><span>Liste des intervenants</span></h1>
			<table class="intervenants-table">
    <thead>
        <tr>
            <th>Nom</th>
			<th>Prenom</th>
			<th>Club</th>
			<th>Fonction</th>
			<th>Contrat</th>
        </tr>
    </thead>
    <tbody>

        	<?php 
			foreach($lesIntervenants->getIntervenants() as $intervenant){ ?>	 
		<tr>
                <td><?php echo $intervenant->getNom() ?></td>
				<td><?php echo $intervenant->getPrenom() ?></td>
				<td><?php echo $intervenant->getClub()->getNOMCLUB() ?></td>
                <td><?php echo $intervenant->getLIBELLE() ?></td>
				<td> <a href="?m2lMP=contratModif">Voir Contrat(s)</a> </td>
            </tr>
		<?php } ?>	
    </tbody>
</table>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>