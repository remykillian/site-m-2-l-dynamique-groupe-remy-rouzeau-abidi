<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
		<div class='listeIntervenant'>
			<h1><span>Liste des contrats</span></h1>
			<?php
				echo $leMenuIntervenants;?>
		<style>
        .center-container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 20vh;
        }
        .form-container {
            text-align: center;
            background-color: #f0f0f0;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }    
		</style>
			<?php
    if (isset($_GET['actionContrat']) && $_GET['actionContrat'] == "Ajouter"){ 
		$intervenant = $lesIntervenants->chercheIntervenant($_SESSION['intervenant']);
		?>
    <div class="center-container">
        <div class="form-container">
					<form action="?actionContrat2=Ajouter" method="post">
					<?php echo('Ajout de contrat pour : '.strtoupper($intervenant->getNom())); ?>
					<br><br>
			<label for="datedebut">Date de debut :</label>
			<input type="date" id="datedebut" name="datedebut" required>
			<br><br>

			<label for="datefin">Date de fin :</label>
			<input type="date" id="datefin" name="datefin" required>
			<br><br>
			<label for="typecontrat">Type de contrat :</label>
			<select id="typecontrat" name="typecontrat" required>
				<option value="CDD">CDD</option>
				<option value="CDI">CDI</option>
				<option value="Autre">Autre</option>
			</select>
			<br><br>

			<label>Nombre d'heures :</label>
			<input type="text" id="nombreheure" name="nombreheure">
			<br><br>

			<input type="submit" value="Ajouter">
			<button onclick="retournerPagePrecedente()">Annuler</button>
			<script>
        function retournerPagePrecedente() {
            window.history.back();
        }
    </script>
		</form>
        </div>
    </div>
			<?php
    } elseif (isset($_GET['actionContrat']) && $_GET['actionContrat'] == "Modifier") {
		$intervenant = $lesIntervenants->chercheIntervenant($_SESSION['intervenant']);
	?>
	    <div class="center-container">
        <div class="form-container">
					<form action="?actionContrat2=Modifier" method="post">
					<?php echo('Modification de contrat pour : '.strtoupper($intervenant->getNom())); ?>
					<br><br>
					<label>Sélectionnez un contrat  :</label>
					<select id="numerocontrat" name="numerocontrat">
            <?php
            $listeContrats = UtilisateurDAO::getContrats($_SESSION['intervenant']);
			foreach ($listeContrats as $Contrat){
                $valeur = $Contrat->getIDCONTRAT();
                echo "<option value='$valeur'>$valeur</option>";
            }
            ?>
        </select> 			<br><br>
			<label for="datedebut">Date de debut :</label>
			<input type="date" id="datedebut" name="datedebut" required>
			<br><br>

			<label for="datefin">Date de fin :</label>
			<input type="date" id="datefin" name="datefin" required>
			<br><br>
			<label for="typecontrat">Type de contrat :</label>
			<select id="typecontrat" name="typecontrat" required>
				<option value="CDD">CDD</option>
				<option value="CDI">CDI</option>
				<option value="Autre">Autre</option>
			</select>
			<br><br>

			<label>Nombre d'heures :</label>
			<input type="text" id="nombreheure" name="nombreheure">
			<br><br>

			<input type="submit" value="Modifier">
			<button onclick="retournerPagePrecedente()">Annuler</button>
			<script>
        function retournerPagePrecedente() {
            window.history.back();
        }
    </script>
		</form>
        </div>
    </div>
	<?php }
     elseif (isset($_GET['actionContrat']) && $_GET['actionContrat'] == "Supprimer") {
		$intervenant = $lesIntervenants->chercheIntervenant($_SESSION['intervenant']);
	?>
	    <div class="center-container">
        <div class="form-container">
					<form action="?actionContrat2=Supprimer" method="post">
					<?php echo('Suppression de contrat pour : '.strtoupper($intervenant->getNom())); ?>
					<br><br>
					<label>Sélectionnez un contrat  :</label>
					<select id="numerocontrat" name="numerocontrat">
            <?php
            $listeContrats = UtilisateurDAO::getContrats($_SESSION['intervenant']);
			foreach ($listeContrats as $Contrat){
                $valeur = $Contrat->getIDCONTRAT();
                echo "<option value='$valeur'>$valeur</option>";
            }
            ?>
        </select> <br><br>
			<input type="submit" value="Supprimer">
			<button onclick="retournerPagePrecedente()">Annuler</button>
			<script>
        function retournerPagePrecedente() {
            window.history.back();
        }
    </script>
		</form>
        </div>
    </div>
	<?php } ?>
	<?php
	if (isset($formulaireContrat)){
        $formulaireContrat->afficherFormulaire();
	}
	?>
	</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>
