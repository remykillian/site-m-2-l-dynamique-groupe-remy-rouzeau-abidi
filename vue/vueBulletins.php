<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
		<div class='listeIntervenant'>
			<h1><span>Liste des bulletins</span></h1>
			<table class="intervenants-table">
    <tbody>

        	<?php 
                if (!isset($_GET['getBulletin'])){ 
                    ?>

                    <thead>
                    <tr>
                        <th>Contrat</th>
                        <th>Contractant</th>
                        <th>Bulletins</th>
                    </tr>
                </thead>
            <?php 
                    $intervenant = $lesIntervenants->chercheIntervenant($_SESSION['intervenant']);
                          
			foreach($lesIntervenants->getIntervenants() as $intervenant){ 
            $listeContrats = UtilisateurDAO::getContrats($intervenant->getIdUser());
            if (!empty($listeContrats)){
                foreach ($listeContrats as $Contrat){
            ?>
		<tr>
                <td><?php echo "n°". $Contrat->getIDCONTRAT() ?></td>
				<td><?php echo $intervenant->getNom()." ".$intervenant->getPrenom() ?></td>
                <td><a href="?getBulletin=<?php echo $Contrat->getIDCONTRAT(); ?>">Voir Bulletin(s)</a></td>
               </tr>
		<?php } } } } else { 
            $listeBulletins = UtilisateurDAO::getBulletins($_GET['getBulletin']);
            if (!empty($listeBulletins)){
                foreach ($listeBulletins as $Bulletin){
                    ?>
                                  <thead>
                                  <h1><span>Contrat n° <?php echo($_GET['getBulletin']) ?> </span></h1>
                    <tr>
                        <th>Numero Bulletin</th>
                        <th>Mois</th>
                        <th>Annee</th>
                        <th>PDF</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <td><?php echo "n°". $Bulletin->getIDBULLETIN() ?></td>
				<td><?php echo $Bulletin->getMOIS()?></td>
				<td><?php echo $Bulletin->getANNEE()?></td>
				<td> <a href="fichier.pdf">Voir PDF</a> </td>
                <td> <form action="Modifier" method="get">
                <input type="submit" value="Modifier"> 
    </form>
    <form action="Supprimer" method="get">
                <input type="submit" value="Supprimer"> 
    </form>

    </td>
                       <?php } ?> 
                       <form action="Ajouter" method="get">
                <input type="submit" value="Ajouter un bulletin"> 
        </form>
                       <?php } 
            else{
                echo("aucun bulletin trouvé");
                
            } ?> 
			<button onclick="retournerPagePrecedente()">Retour</button>
			<script>
        function retournerPagePrecedente() {
            window.history.back();
        }
    </script>
<?php } ?>
    </tbody>
</table>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>