<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main <?php if($_SESSION['authentification'] == 'SEC') { echo 'style="margin-top: 250px;"'; } ?>>
		<div class='gauche'>
			<?php include 'vue/locaux/locauxGauche.php' ;?>
		</div>
		<div class='droite'>
			<?php include 'vue/locaux/locauxDroite.php' ;?>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>